#/bin/bash

file="${1}"
type=$(cat "${file}" | sed 's/\r$//' | sed -n 's/^[\`]\{3\}\([^\\n\\r]\+\)/\1/p' | head -n 1 | tr -d '\n')

url="${KROKI_URL}/${type}/svg"

svg=$(awk -f /var/latex/markdown-code-block-extractor.awk "${file}" | curl -X POST -H "Content-Type: text/plain" -H "Accept: image/svg+xml" --data-binary @- "${url}")

# remove fallbacks coz inkscape doesnt support it
# svg=$(echo "${svg}" | sed "s/\(font-family=\"\)\([^,\"]\+\)[^\"]*/\1\2/g")

# excalidraw only writes out css aliases as font names...
svg=$(echo "${svg}" | sed -e "s/Cascadia/'Cascadia Code'/g")
svg=$(echo "${svg}" | sed -e "s/Virgil/'Virgil 3 YOFF'/g")

dir=$(dirname "${file}")
file_name=$(basename "${file}")
output="svg-inkscape/markdown/${dir}"

mkdir -p "${output}"
echo "${svg}" > "${output}/${file_name}.svg"
