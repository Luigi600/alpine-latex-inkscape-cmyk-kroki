#!/usr/bin/env -S gawk -f
# based on https://gist.github.com/vmx/e4a01e3419fbe1cb77e96ea4ce8b9669

BEGIN {
    in_code_block = 0;
}
/^```/ {
   if (in_code_block == 0) {
       in_code_block = 1
   } 
   else {
       in_code_block = 0
   }
   next
}
in_code_block {
   print
}
