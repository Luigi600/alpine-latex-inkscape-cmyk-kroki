ARG BASE_IMAGE_VERSION=1.0.7

# -------------------------------------------------------------------------------

FROM alpine/git:2.43.0 AS excalidraw-fonts

RUN git clone --depth 1 --branch v0.17.3 https://github.com/excalidraw/excalidraw.git /data



# -------------------------------------------------------------------------------



FROM vanekt/ttf2woff2:1.0 AS font-converter

WORKDIR /work

COPY --from=excalidraw-fonts /data/public/Assistant* ./Assistant/
COPY --from=excalidraw-fonts /data/public/Cascadia*  ./Cascadia/
COPY --from=excalidraw-fonts /data/public/FG_Virgil* ./FG_Virgil/

RUN find ./ -name '*.woff2' -exec woff2_decompress {} \;

# move to a separate directory to copy the fonts with subdirectory structure but without woff2 files (later via Docker)
RUN find ./ -name '*.ttf' -exec bash -c 'file="$@" && dir=$(dirname "${file}") && mkdir -p "/converter/${dir}" && mv "${file}" "/converter/${file}"' bash {} \;



# -------------------------------------------------------------------------------



FROM registry.gitlab.com/luigi600/alpine-latex-inkscape:$BASE_IMAGE_VERSION

# these files should change the least, therefore the layers should occur earlier than the others
RUN mv /usr/bin/pdfconverter /usr/bin/pdfconverter_original
ADD markdown-code-block-extractor.awk /var/latex/markdown-code-block-extractor.awk
ADD --chmod=555 pdfconverter.sh /usr/bin/pdfconverter

COPY --from=font-converter /converter/ /usr/share/fonts/

RUN fc-cache -f

ADD --chmod=555 kroki.sh /usr/bin/kroki
