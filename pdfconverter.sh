#!/bin/bash

# source: https://unix.stackexchange.com/a/50695
[[ "${ENABLE_KROKI}" == "true" && ! -z "${MARKDOWN_IMAGES}" && ! -z "${KROKI_URL}" ]] && find ${MARKDOWN_IMAGES}/ -name '*.md' -exec bash -c '/usr/bin/kroki "$@"' bash {} \;

/usr/bin/pdfconverter_original "$@"
